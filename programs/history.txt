07-02-2019: Version 4.7.0

- require ovidentia 8.6.97
- Changed dbprefix from "app_" to "applications_"

01-08-2016: Version 4.6.0

- require ovidentia 8.4.92
- Add security verifications for CSRF

02-12-2013: Version 3.6
- Added configuration page.
- Application url can now be routed through ovidentia to include in xlinks in statistics.
- OvML containers return the new variable <OVApplicationDirectUrl> that contains the original url of the application. 

30-05-2011: Version 3.3
- Remove Type=MyIsam when created table.

28-03-2011: Version 3.2
- Fixed wrong positioning of applicationsRoot node in sitemap.


02-08-2010: Version 3.0
- sitemap integration
- application functionality


05-02-2010: Version 2.9
- $babDB->errorManager(false); remplace $babDB->bab_database(false, 'mysql');

15/09/2009: Version 2.8
- Add access control to iframe page
- Add access control to rights page

04/02/2009: Version 2.6
- Update of the mask (Sitemap's Ovidentia) after creation or modification of a link
- Correction for compatibility with PHP5
	
17/03/2008: Version 2.5
- Correction in containers OVML : There was no test on access rights

09/03/2008: Version 2.4
- Correction in containers OVML : the url of the applications showing off in the body of the page were incorrect

18/02/2008: Version 2.3
- Minor correction in fonctionsgen.php: management of the requests written in capital letters

11/02/2008: Version 2.2
- SQL error in the recording of an application if the data contained an apostrophe
- Addition of icons for all the types of applications in the list of the applications
- Replacement of the translation Groups by Rights
- Addition of containers OVML to list the applications
- Addition of the files base.php and index.html to increase the security

18/04/2006: Version 2.0
- Acces control list bug in section

14/04/2006: Version 1.9
- Acces control list bug in section

07/03/2006: Version 1.8
- Correction : meme les liens dans la fenetre courante s'ouvraient en nouvelles fenetres

19/12/2005: Version 1.7
- Correction du bug des nouvelles fenetres par la section utilisateur
- Modifications du Html
