<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once $GLOBALS['babInstallPath'] . 'utilit/urlincl.php';



/**
 * Translates the string.
 *
 * @param string $str
 * @return string
 */
function applications_translate($str)
{
    $translation = bab_translate($str, 'applications');

    return $translation;
}



/**
 * Returns the link mode used for application url one of 'direct' or 'indirect'.
 *
 * @return string	The link mode used for application url.
 */
function applications_getUrlLinkMode()
{
    static $linkMode = null;
    if (!isset($linkMode)) {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/applications');
        $linkMode = $registry->getValue('linkMode', 'direct');
    }

    return $linkMode;
}




/**
 * Sets the link mode used for application url.
 *
 * @param int	$linkMode		The link mode used for application url ('direct' or 'indirect').
 */
function applications_setUrlLinkMode($linkMode)
{
    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/applications');
    $registry->setKeyValue('linkMode', $linkMode);
}




/**
 * Returns the specified url redirected through ovidentia via tg=link.
 *
 * @param string $url
 * @return string
 */
function applications_getRedirectedUrl($url)
{
    $redirectedUrl = $GLOBALS['babUrl'];
    $redirectedUrl = bab_url::mod($redirectedUrl, 'tg', 'link');
    $redirectedUrl = bab_url::mod($redirectedUrl, 'url', $url);

    return $redirectedUrl;
}



/**
 * Returns the specified url redirected through ovidentia or not according
 * to the addon global configuration.
 *
 * @param string $url
 * @return string
 */
function applications_getUrlAccordingToConfiguration($url)
{
    global $babUrl;
    if (applications_getUrlLinkMode() === 'direct' || substr($url, 0, strlen('?')) === '?' || substr($url, 0, strlen($babUrl)) === $babUrl) {
        return $url;
    }

    return applications_getRedirectedUrl($url);
}
