<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';


class Func_Applications extends bab_functionality {

	const	LINK			= '';
	const	NEW_WINDOW		= 'new_window';
	const	IFRAME			= 'iframe';
	const	BODY			= 'body';


	const	APP_SECTION		= 1;
	const	USER_SECTION	= 2;
	const	OVML			= 3;
	const	SITEMAP			= 4;


	public function getDescription() {
		return applications_translate('Applications manipulations interface');
	}



	/**
	 * Create a new application
	 *
	 * @param	string	$url
	 * @param	string	$shortname
	 * @param	string	$longname
	 * @param	string	$description
	 * @param	string	$mode			Func_Applications::LINK | Func_Applications::NEW_WINDOW | Func_Applications::IFRAME | Func_Applications::BODY
	 * @param	int		$place			Func_Applications::APP_SECTION | Func_Applications::USER_SECTION | Func_Applications::OVML | Func_Applications::SITEMAP
	 *
	 * @return int
	 */
	public function createApplication($url, $shortname, $longname, $description, $mode = Func_Applications::LINK, $place = Func_Applications::SITEMAP, $portlet = false)
	{
		if ('' === $url) {
			throw new application_Exception(applications_translate('ERROR in url format'));
		}

		if ('' === $shortname) {
			throw new application_Exception(applications_translate('ERROR the short name is mandatory'));
		}

		global $babDB;

		$res = $babDB->db_query(
		    "
			INSERT INTO applications_list
				(
					url,
					shortdesc,
					longdesc,
					description,
					display_mode,
					place,
					portlet,
					lastupdate
				)
			VALUES
				(
					".$babDB->quote($url).",
					".$babDB->quote($shortname).",
					".$babDB->quote($longname).",
					".$babDB->quote($description).",
					".$babDB->quote($mode).",
					".$babDB->quote($place).",
					".$babDB->quote($portlet).",
					NOW()
				)
		"
		);

		return $babDB->db_insert_id($res);
	}
	
	
	/**
	 * Update application
	 * 
	 */
	public function updateApplication($id_app, $url, $shortname, $longname, $description)
	{
	    global $babDB;
	    
	    $res = $babDB->db_query(
	        "
			UPDATE applications_list
				SET
					url=".$babDB->quote($url).",
					shortdesc=".$babDB->quote($shortname).",
					longdesc=".$babDB->quote($longname).",
					description=".$babDB->quote($description)."
	        WHERE 
	           id=".$babDB->quote($id_app)."
		");
	    
	    return true;
	}


	/**
	 * Get application object by its id.
	 *
	 * @param	int		$id_application
	 * @return application_Item
	 */
	public function getApplication($id_application)
	{


		global $babDB;
		$res = $babDB->db_query("SELECT * from applications_list where id=".$babDB->quote($id_application));
		$arr = $babDB->db_fetch_assoc($res);

		if (!$arr) {
			return null;
		}

		$applicationItem = new application_Item();
		$applicationItem->id = $id_application;
		$applicationItem->setUrl($arr['url']);
		$applicationItem->shortname = $arr['shortdesc'];
		$applicationItem->longname = $arr['longdesc'];
		$applicationItem->description = $arr['description'];
		$applicationItem->mode = $arr['display_mode'];
		$applicationItem->place = $arr['place'];
		$applicationItem->portlet = $arr['portlet'];

		return $applicationItem;
	}



	/**
	 * Get application object based on this url.
	 *
	 * @param	string		$url		The url to search.
	 * @return application_Item
	 */
	public function getApplicationByUrl($url)
	{


		global $babDB;
		$res = $babDB->db_query("SELECT * from applications_list where url=".$babDB->quote($url));
		$arr = $babDB->db_fetch_assoc($res);

		if (!$arr) {
			return null;
		}

		$obj = new application_Item;
		$obj->id = $arr['id'];
		$obj->url = applications_getUrlAccordingToConfiguration($arr['url']);
		$obj->shortname = $arr['shortdesc'];
		$obj->longname = $arr['longdesc'];
		$obj->description = $arr['description'];
		$obj->mode = $arr['display_mode'];
		$obj->place = $arr['place'];
		$obj->portlet = $arr['portlet'];

		return $obj;
	}



	/**
	 * Delete application by ID
	 * @param	int		$id_application
	 * @return bool
	 */
	public function deleteApplication($id_application)
	{
		global $babDB;
		$res = $babDB->db_query("delete from applications_list where id=".$babDB->quote($id_application));

		if ($res) {
			return true;
		}

		return false;
	}


	/**
	 * Get access url
	 *
	 * @param	int		$id_application
	 * @param	string	$backurl			optional url to return to
	 *
	 * @return  bab_url
	 */
	public function getAccessUrl($id_application, $backurl = null)
	{
		require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';

		$addon = bab_getAddonInfosInstance('applications');
		$url = new bab_url($addon->getUrl()."main&idx=grp");
		$url->id_app = $id_application;

		if (isset($backurl)) {
			$url->backurl = $backurl;
		}

		return $url;
	}



	/**
	 * Get access url
	 *
	 * @param	int		$id_application
	 *
	 *
	 * @return  string
	 */
	public function getRightsString($id_application)
	{
		require_once $GLOBALS['babInstallPath'].'admin/acl.php';

		return aclGetRightsString('applications_groups', $id_application);
	}
	
	
	/**
	 * Set access rights to an application
	 * @param	int		$id_application
	 * @param	string	$str
	 */
	public function setRightsString($id_application, $str)
	{
		require_once $GLOBALS['babInstallPath'].'admin/acl.php';
	
		aclSetRightsString('applications_groups', $id_application, $str);
	}


	/**
	 * Set default access rights to an application
	 * this method should give access to the current user at least
	 */
	public function setAccess($id_application)
	{
		require_once $GLOBALS['babInstallPath'].'admin/acl.php';

		aclSetGroups_registered('applications_groups', $id_application);
	}
}



class application_Exception extends Exception
{
}


/**
 * Application
 */
class application_Item
{
	/**
	 *
	 * @var int
	 */
	public $id;


	/**
	 *
	 * @var string
	 */
	public $url;

	/**
	 * The direct application's url (without redirection).
	 * @var string
	 */
	public $directurl;

	/**
	 *
	 * @var string
	 */
	public $shortname;


	/**
	 *
	 * @var string
	 */
	public $longname;

	/**
	 *
	 * @var string
	 */
	public $description;

	/**
	 * Func_Applications::NEW_WINDOW | Func_Applications::IFRAME | Func_Applications::BODY
	 * @var string
	 */
	public $mode;


	/**
	 * Func_Applications::APP_SECTION | Func_Applications::USER_SECTION | Func_Applications::OVML | Func_Applications::SITEMAP
	 * @var int
	 */
	public $place;


	/**
	 * @var bool
	 */
	public $portlet;


	/**
	 * Save changes to database
	 * @return bool
	 */
	public function save()
	{
		global $babDB;

		if ('' === $this->url) {
			throw new application_Exception(applications_translate('ERROR in url format'));
		}

		if ('' === $this->shortname) {
			throw new application_Exception(applications_translate('ERROR the short name is mandatory'));
		}


		$res = $babDB->db_query(
		    "
			UPDATE applications_list SET
				url			= ".$babDB->quote($this->url).",
				shortdesc	= ".$babDB->quote($this->shortname).",
				longdesc	= ".$babDB->quote($this->longname).",
				description	= ".$babDB->quote($this->description).",
				display_mode= ".$babDB->quote($this->mode).",
				place		= ".$babDB->quote($this->place).",
				portlet		= ".$babDB->quote($this->portlet).",
				lastupdate	= NOW()
			WHERE
				id=".$babDB->quote($this->id)
		);

		if ($res) {
			bab_sitemap::clearAll();
			return true;
		}

		return false;
	}



	/**
	 * Returns the application url redirected through ovidentia or not according
	 * to the Applications addon global configuration.
	 * @return string
	 */
	public function getUrl()
	{
	    return applications_getUrlAccordingToConfiguration($this->url);
	}
	
	/**
	 * Sets the application url.
	 * @param string $url
	 */
	public function setUrl($url)
	{
	    $this->directurl = $url;
	    $this->url = applications_getUrlAccordingToConfiguration($url);
	}
}
