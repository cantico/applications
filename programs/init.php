<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';




function applications_onSectionCreate(&$title, &$content)
{
	global $babBody;
	static $nbSections=0;

	include_once $GLOBALS['babInstallPath'].'utilit/devtools.php';

	$api = new bab_myAddonSection();
	$list = $api->addElement('list');

	global $babDB;
	$db = $babDB;

	$res = $db->db_query("SELECT a.* from applications_list a WHERE a.place='1' GROUP BY a.id ORDER BY shortdesc,lastupdate ASC ");
	$count = 0;

	if (false == $res) {
		return false;
	}

	while ($arr = $db->db_fetch_array($res)) {

		if (!bab_isAccessValid('applications_groups',$arr['id'])) {
			continue;
		}
		switch($arr['display_mode'])
		{
			case 'new_window':
				$api->pushHtmlData($list, $arr['shortdesc'], array('target'=>'_blank', 'href'=>applications_getUrlAccordingToConfiguration($arr['url']), 'title'=>$arr['longdesc']));
				break;
			case 'iframe':
				$api->pushHtmlData($list, $arr['shortdesc'], array( 'href'=> $GLOBALS['babAddonUrl'].'main&idx=iframe&id_app='.$arr['id'], 'title'=>$arr['longdesc']));
				break;
			default:
			case 'body':
				$api->pushHtmlData($list, $arr['shortdesc'], array( 'href'=>applications_getUrlAccordingToConfiguration($arr['url']), 'title'=>$arr['longdesc']));
				break;
		}

		$count++;
	}

	$title = applications_translate('My applications');
	$content = $api->getHtml();

	if ($nbSections == 0 && ($count > 0)) {
		$nbSections++;
		return true;
	}
    return false;
}




function applications_upgrade($version_base, $version_ini)
{
	$babBody = bab_getBody();
	$db = bab_getDB();

	// db prefix was changed from 'app_' to 'applications_'
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'app_list'"));
	if ($arr[0] == 'app_list') {
	    $db->db_query("RENAME TABLE `app_list` TO `applications_list`");
	    bab_installWindow::message(bab_toHtml(applications_translate('Table `app_list` renamed to `applications_list`')));
	}
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'app_groups'"));
	if ($arr[0] == 'app_groups') {
	    $db->db_query("RENAME TABLE `app_groups` TO `applications_groups`");
	    bab_installWindow::message(bab_toHtml(applications_translate('Table `app_groups` renamed to `applications_groups`')));
	}

	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'applications_list'"));
	if ($arr[0] != 'applications_list') {
		$res[] = $db->db_query(
		    "CREATE TABLE `applications_list` (
			  `id` int(11) unsigned NOT NULL auto_increment,
			  `url` tinytext NOT NULL,
			  `shortdesc` varchar(127) NOT NULL default '',
			  `longdesc` varchar(255) NOT NULL default '',
			  `description` text NOT NULL,
			  `display_mode` enum('new_window','body','iframe') NOT NULL default 'new_window',
			  `place` int(11) unsigned NOT NULL,
			  `lastupdate` datetime NOT NULL default '0000-00-00 00:00:00',
			  PRIMARY KEY  (`id`)
			)"
        );
	} else {
		/* Le champ place remplace l'ancien champ addonsection */
		/* Verifie si le champ addonsection existe */
		$arr = $db->db_fetch_array($db->db_query("show columns from applications_list like 'addonsection'"));
		if ($arr[0] == 'addonsection') {
			/* Le champ existe donc on cree le nouveau champ place */
			$db->db_query("alter table applications_list add place int(11)");
			/* Liste toutes les valeurs de addonsection pour les placer dans place */
			$requete = 'select id, addonsection from applications_list';
			$idrequete = 0;
			$erreurs = array();
			$donnees = applications_sql($requete, $erreurs, $idrequete);
			for ($i = 0; $i <= count($donnees) - 1; $i++) {
				$idapp = $donnees[$i]['id'];
				switch($donnees[$i]['addonsection']) {
					case 'Y':
						$requete2 = "update `applications_list` set `place`=1 where id=$idapp";
						applications_sql($requete2, $erreurs, $idrequete);
						break;
					case 'N':
						$requete2 = "update `applications_list` set `place`=2 where id=$idapp";
						applications_sql($requete2, $erreurs, $idrequete);
						break;
					default:
						$requete2 = "update `applications_list` set `place`=3 where id=$idapp";
						applications_sql($requete2, $erreurs, $idrequete);
						break;
				}
			}
			/* Supprime le champ addonsection */
			$db->db_query("alter table applications_list drop column addonsection");
		} else {
			/* Si le champ addonsection n'existe pas, il faut verifier si le champ place existe car addonsection est deja un champ qui a ete rajoute par la suite */
			$arr = $db->db_fetch_array($db->db_query("show columns from applications_list like 'place'"));
			if ($arr[0] != 'place') {
				/* On cree le champ */
				$db->db_query("alter table applications_list add place int(11)");
				/* On associe a toutes les applications la valeur 1 */
				$db->db_query("update `applications_list` set `place`=1");
			}
		}
	}

	if (!bab_isTableField('applications_list', 'portlet')) {
		$db->db_query("ALTER TABLE `applications_list` ADD `portlet` BOOL NOT NULL DEFAULT false AFTER `place`");
	}

	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'applications_groups'"));
	if ($arr[0] != 'applications_groups') {
		$res[] = $db->db_query(
		    "CREATE TABLE `applications_groups` (
				  `id` int(11) unsigned NOT NULL auto_increment,
				  `id_object` int(11) unsigned NOT NULL default '0',
				  `id_group` int(11) unsigned NOT NULL default '0',
				  UNIQUE KEY `id` (`id`),
				  KEY `id_object` (`id_object`),
				  KEY `id_group` (`id_group`)
			)"
        );
	}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE applications_list display_mode"));
	if ($arr[0] != 'display_mode') {
		$newwindow = array();
		$obj = $db->db_query("SELECT id FROM `applications_list` WHERE newwindow='Y'");
		while ($arr = $db->db_fetch_array($obj)) {
			$newwindow[] = $arr['id'];
		}
		$res[] = $db->db_query("ALTER TABLE `applications_list` CHANGE `newwindow` `display_mode` ENUM( 'new_window', 'body', 'iframe' ) DEFAULT 'new_window' NOT NULL");
		$db->db_query("UPDATE `applications_list` SET display_mode = 'new_window' WHERE id IN (".implode(',',$newwindow).")");
		$db->db_query("UPDATE `applications_list` SET display_mode = 'body' WHERE id NOT IN (".implode(',',$newwindow).")");
	}

	if (isset($res) && is_array($res)) {
		foreach ($res as $value) {
			if (!$value) {
				$babBody->msgerror = applications_translate("ERROR in database upgrade")." : ".$version_base." => ".$version_ini;
				return false;
			}
		}
	}


	// attach events
	include_once $GLOBALS['babInstallPath']."utilit/eventincl.php";

	$addon = bab_getAddonInfosInstance('applications');
	$addonPhpPath = $addon->getPhpPath();

	$addon->removeAllEventListeners();

	$addon->addEventListener(
        'bab_eventGroupDeleted',
        'applications_onGroupDeleted',
        $addonPhpPath.'events.php'
    );
	$addon->addEventListener(
        'bab_eventBeforeSiteMapCreated',
        'applications_onBeforeSiteMapCreated',
        $addonPhpPath.'events.php'
    );



	@bab_functionality::includefile('PortletBackend');
	if (class_exists('Func_PortletBackend')) {

		require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
		$functionalities = new bab_functionalities();
		$functionalities->registerClass('Func_PortletBackend_applications', $addonPhpPath . 'portletbackend.class.php');
	}

	require_once $GLOBALS['babInstallPath'] . 'utilit/functionalityincl.php';
	$oFunctionalities = new bab_functionalities();

	if (false !== $oFunctionalities->register('Applications', $addonPhpPath . 'applications.class.php')) {
		return true;
	}
	//$oFunctionalities->register('Func_SitemapEditorNodeSection_applications', dirname(__FILE__).'/nodesection.class.php');

	return false;
}







function applications_onDeleteAddon()
{
    $addon = bab_getAddonInfosInstance('applications');

	// detach events
    $addon->removeAllEventListeners();

	return true;
}
