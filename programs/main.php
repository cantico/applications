<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


require_once dirname(__FILE__) . '/functions.php';


function applications_favorites_list()
{
	global $babBody;

	class applications_favorites_list_template
	{
		public $db;
		public $res;
		public $count;
		public $favorites;
		public $bmurl;
		public $bmtext;
		public $bmdelurl;
		public $bmdeltext;
		public $alternate = true;

		public function __construct()
		{
			$this->t_applications = applications_translate("Applications");
			$this->t_mod = applications_translate("Edit");
			$this->t_grp = applications_translate('Rights');
			$this->t_newwindow = '';
			$this->t_displaymode = '';
			$this->db = $GLOBALS['babDB'];
			$tab = bab_getUserGroups();
			$this->admin = bab_isUserAdministrator();
			$tab['id'][] = 0;
			if ($GLOBALS['BAB_SESS_LOGGED']) {
			    $tab['id'][] = 1;
			} else {
			    $tab['id'][] = 2;
			}
			$admin = (!$this->admin) ? ", applications_groups g WHERE g.id_object=a.id AND g.id_group IN(".implode(',',$tab['id']).")" : '';
			$req = "select a.* from applications_list a ".$admin." ORDER BY shortdesc,lastupdate ASC";
			$this->res = $this->db->db_query($req);
			$this->count = $this->db->db_num_rows($this->res);
			$index = substr($GLOBALS['babPhpSelf'], 0, 1) == '/' ? substr($GLOBALS['babPhpSelf'], 1) : $GLOBALS['babPhpSelf'];
			$this->r = $GLOBALS['babUrl'].$index;
			$this->imgname = '';
		}

		public function getnext()
		{
			static $i = 0;
			if ($i < $this->count) {
				$this->alternate = !$this->alternate;
				$arr = $this->db->db_fetch_array($this->res);
				$this->description = bab_toHtml($arr['description']);
				$this->longdesc = bab_toHtml($arr['longdesc']);
				$this->shortdesc = bab_toHtml($arr['shortdesc']);
				$this->url = bab_toHtml(applications_getUrlAccordingToConfiguration($arr['url']));
				$this->url_iframe = $GLOBALS['babAddonUrl']."main&idx=iframe&id_app=".$arr['id'];
				$this->grpurl = $GLOBALS['babAddonUrl']."main&idx=grp&id_app=".$arr['id'];
				$this->modurl = $GLOBALS['babAddonUrl']."main&idx=mod&id_app=".$arr['id'];
				$this->display_mode = bab_toHtml($arr['display_mode']);
				switch($this->display_mode) {
					case 'new_window':
						$this->t_newwindow = applications_translate('New window');
						$this->imgname = 'newwindow.png';
						$this->t_displaymode = applications_translate('New window');
						break;
					case 'iframe':
						$this->t_newwindow =applications_translate('In Ovidentia body');
						$this->imgname = 'body.png';
						$this->t_displaymode = applications_translate('In Ovidentia body');
						break;
					default:
					case 'body':
						$this->t_newwindow = applications_translate('Same window');
						$this->imgname = 'iframe.png';
						$this->t_displaymode = applications_translate('Same window');
						break;
				}
				$i++;
				return true;
			} else {
			    return false;
			}
		}
	}

	$temp = new applications_favorites_list_template();
	$babBody->babecho(bab_printTemplate($temp, $GLOBALS['babAddonHtmlPath']."main.html", "bmlist"));
}





function applications_favorites_mod($id, $curr, $lvlone, $lvltwo)
{
	global $babBody;

	class applications_favorites_mod_template
	{
		public $url;
		public $description;
		public $add;

		public function __construct($id, $curr, $lvlone, $lvltwo)
		{
			$this->db = $GLOBALS['babDB'];
			$this->t_url = applications_translate("Url");
			$this->t_description = applications_translate("Description");
			$this->t_add = applications_translate("Record");
			$this->t_del = applications_translate("Delete");

			$this->t_yes = applications_translate("Yes");
			$this->t_no = applications_translate("No");
			$this->t_new_window = applications_translate("New window");
			$this->t_body = applications_translate("Same window");
			$this->t_iframe = applications_translate("In Ovidentia body");
			$this->t_display_mode = applications_translate("Display mode");
			$this->t_longdesc = applications_translate("Long name");
			$this->t_shortdesc = applications_translate("Short name");

			$this->t_position = applications_translate("Place");
			$this->t_in_section = applications_translate("In specific section");
			$this->t_in_user = applications_translate("In user section");
			$this->t_in_nothing = applications_translate('No place');
			$this->t_in_sitemap = applications_translate('In sitemap');

			$this->t_portlet = applications_translate('Available in portlet');
			
			$this->js_alert1 = applications_translate('You must fill the short name');
			$this->js_alert2 = applications_translate('You must fill the url');
			$this->js_alert3 = applications_translate('Do you really whant to delete this bookmark')." ?";

			$this->test = "test";
			$this->id = bab_toHtml($id);

			if (count($_POST)) {
				$this->description = bab_toHtml($_POST['description']);
				$this->longdesc = bab_toHtml($_POST['longdesc']);
				$this->shortdesc = bab_toHtml($_POST['shortdesc']);
				$this->url = bab_toHtml($_POST['url']);
				$this->display_mode = bab_toHtml($_POST['display_mode']);
				$this->place = bab_toHtml($_POST['place']);
				$this->portlet = bab_toHtml($_POST['portlet']);
			} elseif (!$this->id) {
				$this->url = $curr == 1 ? $GLOBALS['HTTP_REFERER'] : '';
				$this->shortdesc = ''.bab_toHtml(urldecode($lvltwo));
				$this->place = 1;
				if ($this->shortdesc != '') {
					$plus =  " - ".bab_toHtml(urldecode($lvltwo));
				} else {
					$plus = '';
				}
				$this->longdesc = ''.bab_toHtml(urldecode($lvlone)).$plus;
				$this->description = '';
				$this->display_mode = ($curr == 1) ? 'body' : 'new_window';
				$this->portlet = 0;
			} else {
				$arr = $this->db->db_fetch_array($this->db->db_query("SELECT * FROM applications_list WHERE id=".$this->db->quote($this->id)));
				$index = substr($GLOBALS['babPhpSelf'], 0, 1) == '/' ? substr($GLOBALS['babPhpSelf'], 1) : $GLOBALS['babPhpSelf'];
				
				$this->description = bab_toHtml($arr['description']);
				$this->longdesc = bab_toHtml($arr['longdesc']);
				$this->shortdesc = bab_toHtml($arr['shortdesc']);
				$this->url = bab_toHtml($arr['url']);
				$this->display_mode = bab_toHtml($arr['display_mode']);
				$this->place = bab_toHtml($arr['place']);
				$this->portlet = bab_toHtml($arr['portlet']);

				if (!$this->display_mode) {
					$this->display_mode = 'body';
				}
			}
		}
	}

	$temp = new applications_favorites_mod_template($id, $curr, $lvlone, $lvltwo);
	$babBody->babecho(bab_printTemplate($temp, $GLOBALS['babAddonHtmlPath']."main.html", "bm_mod"));
}





/**
 *
 * @param int $id   The application id.
 */
function applications_iframe($id)
{
	global $babBody;

	class applications_iframe_template
	{

		public function __construct($id)
		{
			global $babDB;
			
			$sql = 'SELECT url,shortdesc,longdesc FROM applications_list WHERE id=' . $babDB->quote($id);
			$apps = $babDB->db_query($sql);
			$app = $babDB->db_fetch_array($apps);
			$GLOBALS['babBody']->title = !empty($app['longdesc']) ? $app['longdesc'] : $app['shortdesc'];
			$this->url = applications_getUrlAccordingToConfiguration($app['url']);
		}
	}

	if (!bab_isAccessValid('applications_groups', $id)) {
		$babBody->msgerror = applications_translate('Application access denied');
		return;
	}


	$temp = new applications_iframe_template($id);
	$babBody->babecho(bab_printTemplate($temp, $GLOBALS['babAddonHtmlPath']."main.html", "iframe"));

}





/************************ main ************************/

$idx = bab_rp('idx', 'list');

$action = bab_rp('action', null);

if (isset($action) && bab_isUserAdministrator()) {

	$func = bab_functionality::get('Applications');

	$db = $GLOBALS['babDB'];
	switch ($action)
	{
		case 'delete':
		    bab_requireDeleteMethod();
			if (is_numeric($_POST['id_app'])) {
				$func->deleteApplication($_POST['id_app']);
            }
            break;
		
		case 'modify':
		    
		    bab_requireSaveMethod();

			$id_app 		= bab_pp('id_app');
			$url 			= bab_pp('url');
			$shortdesc 		= bab_pp('shortdesc');
			$longdesc 		= bab_pp('longdesc');
			$description 	= bab_pp('description');
			$display_mode 	= bab_pp('display_mode');
			$place 			= bab_pp('place');
			$portlet 		= bab_pp('portlet');


			try {

    			if ($id_app == '') {
    				$func->createApplication($url, $shortdesc, $longdesc, $description, $display_mode, $place, $portlet);
    			} else {
    				$app = $func->getApplication($id_app);
    				$app->url = $url;
    				$app->shortname = $shortdesc;
    				$app->longname = $longdesc;
    				$app->description = $description;
    				$app->mode = $display_mode;
    				$app->place = $place;
    				$app->portlet = $portlet;
    				$app->save();
    			}

			} catch(application_Exception $e) {
				$idx = 'mod';
				$babBody->msgerror = applications_translate('ERROR in url format');
			}



			/* Mise a jour du plan du site (cache) du noyau Ovidentia afin d'afficher le lien dans les sections */
		    bab_sitemap::clearAll();
			break;
	}
}


$id_app = bab_rp('id_app', false);


switch ($idx)
{
	case 'mod':
		if (!bab_isUserAdministrator()) {
		    break;
		}
		$babBody->title = (empty($id_app)) ? applications_translate('Add application') : applications_translate('Edit application');
		if (!isset($curr)) {
		    $curr = '';
		}
		if (!isset($lvlone)) {
		    $lvlone = '';
		}
		if (!isset($lvltwo)) {
		    $lvltwo = '';
		}
		applications_favorites_mod($id_app, $curr, $lvlone, $lvltwo);
		$babBody->addItemMenu('list', applications_translate('List'), $GLOBALS['babAddonUrl'].'main&idx=list');
		if (!$id_app) {
			$babBody->addItemMenu('mod', applications_translate('Add'), $GLOBALS['babAddonUrl'].'main&idx=mod');
		} else {
			$babBody->addItemMenu('mod', applications_translate('Modify') , $GLOBALS['babAddonUrl'].'main&idx=mod');
			$babBody->addItemMenu('mod2', applications_translate('Add'), $GLOBALS['babAddonUrl'].'main&idx=mod');
		}
		break;

	case 'grp':
		if (!bab_isUserAdministrator()) {
			break;
		}
		$babBody->title = applications_translate('Link access rights');
		include_once $babInstallPath.'admin/acl.php';
		$babBody->addItemMenu('list', applications_translate('List'), $GLOBALS['babAddonUrl'].'main&idx=list');
		$babBody->addItemMenu('grp', applications_translate('Rights'), $GLOBALS['babAddonUrl'].'main&idx=grp');
		if (bab_rp('acl')) {
			maclGroups();

			$backurl = bab_rp('backurl');
			if (!empty($backurl))
			{
				header('location:'.$backurl);
				exit;
			}

			header('location:'.$GLOBALS['babAddonUrl'].'main&idx=list');
			exit;
		} else {
			$macl = new macl(bab_rp('tg'), bab_rp('idx'), $id_app, 'acl', true, 0);
			$macl->addtable('applications_groups');
			$macl->set_hidden_field('backurl', bab_rp('backurl'));
			$macl->babecho();
		}
		break;

	case 'iframe':
		applications_iframe($_GET['id_app']);
		break;

	case 'list':
	default:
		if (!bab_isUserAdministrator()) {
			break;
		}
		$babBody->title = applications_translate('Your favorites');
		applications_favorites_list();
		$babBody->addItemMenu('list', applications_translate('List'), $GLOBALS['babAddonUrl'].'main&idx=list');
		if (bab_isUserAdministrator()) {
		    $babBody->addItemMenu('mod', applications_translate('Add'), $GLOBALS['babAddonUrl'].'main&idx=mod');
		}
		break;
}

$babBody->setCurrentItemMenu($idx);
