<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';


/**
 * Displays the addon configuration page.
 *
 * @param string $message
 */
function applications_configurationMenu($message = null)
{
	global $babBody;

	/* @var $Icons Func_Icons */
	$Icons = bab_Functionality::get('Icons');
	$Icons->includeCss();

	$babBody->setTitle(applications_translate('Applications addon configuration'));

	if (isset($message)) {
		$babBody->addMessage($message);
	}

	$linkMode = applications_getUrlLinkMode();

	$directChecked = ($linkMode == 'direct') ? 'checked="checked"' : '';
	$indirectChecked = ($linkMode == 'direct') ? '' : 'checked="checked"';


	$babBody->babEcho(
	    '
		<div class="widget-bordered" style="width: 90%; margin: auto;">

		<div>
		<form method="POST">
	    <input type="hidden" name="babCsrfProtect" value="' . bab_CsrfProtect::getToken() . '" />
		<input type="hidden" name="tg" value="' . bab_rp('tg') . '" />
		<input type="hidden" name="idx" value="saveConfiguration" />

	    <h3>' . applications_translate('External link management') . ' :</h3>
	    <br />

	    <input id="applications_linkmode_direct" type="radio" ' . $directChecked . ' name="config[linkmode]" value="direct" style="vertical-align: top;" />
	    <div style="display: inline-block">
	    <label for="applications_linkmode_direct" style="vertical-align: middle; font-weight: bold">' . applications_translate('Direct') . '</label><br>
	    <div>' . applications_translate('External links are called directly.') . '</div>
	    <div>' . applications_translate('The portal keeps no tracks of clicks on those links.') . '</div>
	    </div>

	    <br />
	    <br />

	    <input id="applications_linkmode_indirect" type="radio" ' . $indirectChecked . ' name="config[linkmode]" value="indirect" style="vertical-align: top;" />
	    <div style="display: inline-block">
	    <label for="applications_linkmode_indirect" style="vertical-align: top; font-weight: bold">' . applications_translate('Indirect') . '</label><br>
	    <div>' . applications_translate('External links managed by the Applications addon are routed through ovidentia.') . '</div>
	    <div>' . applications_translate('Clicks on those links can be recorded by the portal and included in the statistics.') . '</div>
	    </div>

	    <br />
	    <br />

		<input type="submit" value="' . applications_translate('Save configuration') . '" />
		<input type="reset" value="' . applications_translate('Cancel') . '" />
	    </form>
		</div>
		</div>
	'
	);
}




/**
 * Saves the addon configuration.
 *
 * @param array $config
 * @return boolean
 */
function applications_configurationSave($config)
{

    applications_setUrlLinkMode($config['linkmode']);

    if (class_exists('bab_sitemap')) {
    	bab_sitemap::clearAll();
    }

    return true;
}



$idx = bab_rp('idx');

switch($idx) {

	case 'saveConfiguration':
	    $config = bab_rp('config', null);
	    applications_configurationSave($config);
	    applications_configurationMenu(applications_translate('The configuration has been saved.'));

		break;

	default:
	case 'menu':
		applications_configurationMenu();
		break;
}
