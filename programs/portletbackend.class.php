<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
global $babInstallPath;
include_once $babInstallPath.'admin/acl.php';
require_once dirname(__FILE__) . '/functions.php';

bab_Functionality::includefile('PortletBackend');

/**
 *
 */
class Func_PortletBackend_applications extends Func_PortletBackend
{
	public function getDescription()
	{
		return applications_translate('Portlets from applications addon');
	}
	
	public function select($category = null)
	{
	    $addon = bab_getAddonInfosInstance('applications');
	    
	    if (!$addon->isAccessValid()) {
	        return array();
	    }
	    
	    
		global $babDB;
		$res = $babDB->db_query("SELECT a.* from applications_list a WHERE a.portlet = 1 ORDER BY shortdesc,lastupdate ASC");
		$listApp = array();
		while ($arr = $babDB->db_fetch_array($res)) {
			if (bab_isAccessValid('applications_groups', $arr['id'])) {
				$listApp['applications_'.$arr['id']] = $this->portlet_Applications($arr['id'], $arr['shortdesc'], $arr['description']);
			}
		}
		
		return $listApp;
	}
	
	
	
	/**
	 * Get portlet definition instance
	 * @param	string	$portletId			portlet definition ID
	 *
	 * @return portlet_PortletDefinitionInterface
	 */
	public function getPortletDefinition($portletId)
	{
	    $addon = bab_getAddonInfosInstance('applications');
	    if (!$addon->isAccessValid()) {
	        return null;
	    }
	    
		$portletId = str_replace('applications_', '', $portletId);
		
		global $babDB;
		$res = $babDB->db_query("SELECT a.* from applications_list a WHERE a.portlet = 1 AND id=".$babDB->quote($portletId)." ORDER BY shortdesc,lastupdate ASC");
		$arr = $babDB->db_fetch_array($res);
		if (bab_isAccessValid('applications_groups',$arr['id'])) {
			return $this->portlet_Applications($arr['id'], $arr['shortdesc'], $arr['description']);
		}
		
		return null;
	}


	public function portlet_Applications($id, $name, $description)
	{
		return new PortletDefinition_applications($id, $name, $description);
	}
	
	public function getConfigurationActions()
	{
		return array();
	}
}


/////////////////////////////////////////


class PortletDefinition_applications implements portlet_PortletDefinitionInterface
{
	
	private $id;
	private $name;
	private $description;
	
	public function __construct($id, $name, $description)
	{
		$this->id = 'applications_'.$id;
		$this->name = $name;
		$this->description = $description;
	}

	public function getId()
	{
		return $this->id;
	}
	
	
	public function getName()
	{
		return $this->name;
	}


	public function getDescription()
	{
		return $this->description;
	}


	public function getPortlet()
	{
		return new Portlet_applications($this->id, $this->name, $this->description);
	}
	
	public function getPreferenceFields()
	{
		return array(
		    array(
		        'label' => applications_translate('Size (in px)'),
		        'type' => 'text',
		        'name' => 'iframe_size'
		    )
		);
	}
	
	/**
	 * Returns the widget rich icon URL.
	 * 128x128 ?
	 *
	 * @return string
	 */
	public function getRichIcon()
	{
		return '';
	}
	
	
	/**
	 * Returns the widget icon URL.
	 * 16x16 ?
	 *
	 * @return string
	 */
	public function getIcon()
	{
		return '';
	}
	
	/**
	 * Get thumbnail URL
	 * max 120x60
	 */
	public function getThumbnail()
	{
		return '';
	}
	
	public function getConfigurationActions()
	{
		return array();
	}
}




class Portlet_applications extends Widget_Item implements portlet_PortletInterface
{
	private $id;
	private $name;
	private $description;
	
	/**
	 * Instanciates the widget factory.
	 *
	 * @return Func_Widgets
	 */
	public function Widgets()
	{
		return bab_Functionality::get('Widgets');
	}
	
	/**
	 */
	public function __construct($id, $name, $description)
	{
		$this->id = $id;
		$this->name = $name;
		$this->description = $description;
	}

	/**
	 * @param Widget_Canvas	$canvas
	 * @ignore
	 */
	public function display(Widget_Canvas $canvas)
	{
		$W = $this->Widgets();
		
		$portletId = str_replace('applications_', '', $this->id);
		global $babDB;
		$res = $babDB->db_query("SELECT a.* from applications_list a WHERE a.portlet = 1 AND id=".$babDB->quote($portletId)." ORDER BY shortdesc,lastupdate ASC");
		$arr = $babDB->db_fetch_array($res);
		if (bab_isAccessValid('applications_groups', $arr['id'])) {
			
			$portlet_size = '';
			if ($this->options['iframe_size']) {
				$portlet_size = 'height: '.$this->options['iframe_size'].'px;';
			}
			$this->item = $W->Html('<iframe style="width: 100%; border:none;'.$portlet_size.'" src="'.applications_getUrlAccordingToConfiguration($arr['url']).'"></iframe>');
		} else {
			$this->item = $W->Html('');
		}
		
		return $this->item->display($canvas);
	}

	public function getName()
	{
		return get_class($this);
	}

	public function getPortletDefinition()
	{
		return new PortletDefinition_applications($this->id, $this->name, $this->description);
	}
	
	/**
	 * receive current user configuration from portlet API
	 */
	public function setPreferences(array $configuration)
	{
		$this->options = $configuration;
	}
	
	public function setPreference($name, $value)
	{
		$this->options[$name] = $value;
	}
	
	public function setPortletId($id)
	{
		
	}
}
