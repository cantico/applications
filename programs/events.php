<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/applications.class.php';



function applications_onGroupDeleted($event)
{
	require_once( $GLOBALS['babInstallPath'] . "admin/acl.php");
	aclDeleteGroup('applications_groups', $event->id_group);
}


function applications_onBeforeSiteMapCreated(bab_eventBeforeSiteMapCreated $event)
{
	global $babDB;
	
	bab_Functionality::includefile('Icons');

	$addon_prefix = 'applications';

	
	if (bab_isUserAdministrator()) {

    	// Main entry in user section.
    
    	$item = $event->createItem('applications');
    	$item->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons'));
    	$item->setLabel(applications_translate('Applications'));
    	$item->setLink($GLOBALS['babAddonUrl'].'main');
    	$item->addIconClassname(Func_Icons::PLACES_USER_APPLICATIONS);
    	$event->addFunction($item);

    	//  Main entry in admin section.
    	
    	$item = $event->createItem('applications_administration');
    	$item->setPosition(array('root', 'DGAll', 'babAdmin', 'babAdminSectionAddons'));
    	$item->setLabel(applications_translate('Applications'));
    	$item->setLink($GLOBALS['babAddonUrl'].'configuration');
    	$item->addIconClassname(Func_Icons::PLACES_USER_APPLICATIONS);
    	$event->addFunction($item);
	}
	
	// Add sitemap entries in dedicated sitemap root node.
	$query = 'SELECT *
		FROM applications_list
		WHERE
			`place` = ' . $babDB->quote(Func_Applications::SITEMAP) . '
            AND id IN(' . $babDB->quote(bab_getUserIdObjects('applications_groups')) . ')
	';
	
	$res = $babDB->db_query($query);
	
	if (0 < $babDB->db_num_rows($res)) {

		$folder = $event->createItem($addon_prefix.'Root');
		$folder->setLabel(applications_translate('Applications'));
		$folder->setPosition(array('root', 'DGAll'));
		$folder->addIconClassname(Func_Icons::PLACES_USER_APPLICATIONS);
		$event->addFolder($folder);


		while ($arr = $babDB->db_fetch_assoc($res)) {
			$link = $event->createItem($addon_prefix.'App_'.$arr['id']);
			$link->setLabel($arr['shortdesc']);
			$link->setDescription($arr['longdesc']);
			
			$url = applications_getUrlAccordingToConfiguration($arr['url']);

			switch ($arr['display_mode']) {
				case 'new_window':
					$link->setLink($url, 'bab_popup_obj=0;bab_popup(this.href,10,1);return false');
					break;

				case 'iframe':
					$url = $GLOBALS['babAddonUrl']."main&idx=iframe&id_app=".$arr['id'];
					$link->setLink($url);
					break;

				case 'body':
				default:
				    $link->setLink($url);
					break;
			}

			$link->setPosition(array('root', 'DGAll', $addon_prefix.'Root'));
			$link->addIconClassname('categories-applications-education');
			$event->addFunction($link);
		}
	}


	// Add sitemap entries in user section node.
	
	$query = 'SELECT *
		FROM applications_list
		WHERE
			`place` = ' . $babDB->quote(Func_Applications::USER_SECTION) . '
            AND id IN(' . $babDB->quote(bab_getUserIdObjects('applications_groups')) . ')
	';
	
	$res = $babDB->db_query($query);
	
    while ($arr = $babDB->db_fetch_assoc($res)) {
        $link = $event->createItem($addon_prefix.'App_'.$arr['id']);
        $link->setLabel($arr['shortdesc']);
        $link->setDescription($arr['longdesc']);
        	
        $url = applications_getUrlAccordingToConfiguration($arr['url']);

        switch ($arr['display_mode']) {
        	case 'new_window':
        	    $link->setLink($url, 'bab_popup_obj=0;bab_popup(this.href,10,1);return false');
        	    break;

        	case 'iframe':
        	    $url = $GLOBALS['babAddonUrl']."main&idx=iframe&id_app=".$arr['id'];
        	    $link->setLink($url);
        	    break;

        	case 'body':
        	default:
        	    $link->setLink($url);
        	    break;
        }

        $link->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons'));
        $link->addIconClassname('categories-applications-education');
        $event->addFunction($link);
    }
}
