// <?php
// //-------------------------------------------------------------------------
// // OVIDENTIA http://www.ovidentia.org
// // Ovidentia is free software; you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation; either version 2, or (at your option)
// // any later version.
// //
// // This program is distributed in the hope that it will be useful, but
// // WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// // See the GNU General Public License for more details.
// //
// // You should have received a copy of the GNU General Public License
// // along with this program; if not, write to the Free Software
// // Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// // USA.
// //-------------------------------------------------------------------------
// /**
//  * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
//  * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
//  */
// require_once 'base.php';
// require_once dirname(__FILE__).'/functions.php';

// class Func_SitemapEditorNodeSection_applications extends Func_SitemapEditorNodeSection 
// {
// 	public function getDescription()
// 	{
// 		return smed_translate('Sitemap editor node sections');
// 	}

	
	
// 	/**
// 	 * Get displayable frame for the node editor
// 	 * if the method return NULL the section is not displayed
// 	 *
// 	 * @param	string	$content_type		container | url | category | topic | ref
// 	 * @param	string	$node				function ID of sitemap node, NULL for the edit form of a new node
// 	 *
// 	 * @return Widget_Displayable_Interface
// 	 */
// 	public function getFormSection($content_type, $nodeId = null) {
// 		if($nodeId === null){
// 			return null;
// 		}
// 		$W = bab_Widgets();
// 		return $W->Acl();
// 	}
	
// 	/**
// 	 * Get recorded values for the node Id
// 	 * @param string $nodeId
// 	 * @return array | null
// 	 */
// 	public function getValues($nodeId) {
// 		return null;
// 	}
	
	
// 	/**
// 	 * Save information posted by the form section
// 	 *
// 	 * @param	string	$content_type		container | url | category | topic | ref
// 	 * @param	string	$nodeId				function ID of sitemap node
// 	 * @param	array	$values				posted values of this form section
// 	 */
// 	public function saveFormSection($content_type, $nodeId, $values = array()) {
		
// 	}
	
	
// 	/**
// 	 * Labeled field
// 	 * This method can be used to get the same look in form
// 	 * @param 	string 		$labelText
// 	 * @param 	Widget_Item $item
// 	 * @param 	string 		$fieldName
// 	 * @param 	string 		$description
// 	 * 
// 	 * @return smed_LabelledWidget
// 	 */
// 	protected function labelledField($labelText, Widget_Item $item, $fieldName = null, $description = null)
// 	{
// 		$W = bab_Widgets();
		
// 		$labelledItem = new smed_LabelledWidget($labelText, $item);
		
// 		if (isset($fieldName)) {
// 			$labelledItem->setName($fieldName);
// 		}
		
// 		if (isset($description)) {
// 			$labelledItem->setDescription($description);
// 		}
		
// 		return $labelledItem;
// 	}
// }